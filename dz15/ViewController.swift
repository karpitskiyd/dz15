//
//  ViewController.swift
//  dz15
//
//  Created by Даниил Карпитский on 2/14/22.
//

import UIKit
var scale: Float = 0.0
var rotationDegree:Float = 0.0

class ViewController: UIViewController {
    var image: UIImage!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scaleSlider: UISlider!
    @IBOutlet weak var rotationSlider: UISlider!
    
    
    @IBAction func scaleSliderMoved(_ sender: Any) {
        scale = scaleSlider.value
        imageView.transform = CGAffineTransform(scaleX: CGFloat(scale), y: CGFloat(scale))
    }
    
    @IBAction func rotationSliderMoved(_ sender: Any) {
        imageView.transform = CGAffineTransform(scaleX: CGFloat(scale), y: CGFloat(scale))
        rotationDegree = rotationSlider.value
        imageView.transform = CGAffineTransform(rotationAngle: CGFloat(rotationDegree))
    }
   
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        image = UIImage(named: "Evoque")
        imageView.image = image
        
        
    }


}

